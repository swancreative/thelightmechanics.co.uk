$(document).ready(function() {
  $('.text-group .text-group__button').click(function(event) {
    event.preventDefault();
    if ($(this).parent().parent().hasClass('text-group--open')) {
      $('.text-group').removeClass('text-group--open');
      $('.text-group__content').slideUp();
    } else {
      $('.text-group').removeClass('text-group--open');
      $('.text-group__content').slideUp();
      $(this).parent().parent().addClass('text-group--open');
      $(this).parent().next('.text-group__content').slideDown();
    }
  });

  $('a[href="#contact-us"]').click(function(event) {
    event.preventDefault();
    $('html, body').animate({
      scrollTop: $('.contact').offset().top
    }, 1000);
  });

  $('#form105').on('submit', function(event) {
    var this_form = $(this).attr('id');
    $('#' + this_form + ' .form__error').remove();
    event.preventDefault();
    $.ajax({
      url: 'http://forms.swancreative.com/w/' + $(this).data('wufooformid'),
      data: $(this).serialize(),
      type: 'POST',
      success: function(response) {
        if (response.Success === 1) {
          $('#' + this_form).after('<p class="form__success">Thank you for contacting us!</p>');
          $('#' + this_form).remove();
        } else {
          var i = 0;
          $.each(response.FieldErrors, function() {
            $('#' + this_form + ' #' + response.FieldErrors[i].ID).after('<p class="form__error">' + response.FieldErrors[i].ErrorText + '</p>');
            i++;
          });
        }
      }
    });
  });

  $('.footer__year').text(new Date().getFullYear());
});
